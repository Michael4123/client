﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalProjectClient
{
    class SettingsDC
    {
        public SettingsDC(NodeContent nc)
        {
            NodeContent = nc;
            Settings = new Settings();
        }

        public Settings Settings { get; set; }
        public NodeContent NodeContent { get; set; }
    }
}
