﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalProjectClient
{
    public interface IDataRow
    {
        void set_content(string s, bool even = false);
        string get_content();
    }
}
