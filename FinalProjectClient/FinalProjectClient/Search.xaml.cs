﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using static FinalProjectClient.MainWindow;

namespace FinalProjectClient
{
    /// <summary>
    /// Interaction logic for Search.xaml
    /// </summary>
    public partial class Search : Page
    {

        #region DEF
        Log log;
        searchFunc sf;
        bool isLoading;
        private double SearchTextBoxHeight
        {
            get
            {
                return SearchTextBox.Height;
            }
            set { SearchTextBox.Height = value; }
        }
        public string ImageSource
        {
            get
            {
                if (isLoading) return "Resources/X.png";
                else return "Resources/refresh.png";
            }
        }
        #endregion
        public Search(Log log, searchFunc sf)
        {
            InitializeComponent();
            this.sf = sf;
            DataContext = this;
            SearchTextBox.KeyDown += new KeyEventHandler(tb_KeyDown);
            this.log = log;
        }
        // Handling search query fired by enter pressing
        void tb_KeyDown(object sender, KeyEventArgs e)
        {
            Dispatcher.Invoke(delegate
            {
                tool.ImageSource = new BitmapImage(new Uri("Resources/X.png", UriKind.RelativeOrAbsolute));
            });
            if (e.Key == Key.Enter)
            {
                isLoading = true;
                /* 
                 * As for now I delete it
                 * (Were my own controllers)
                 * tool.ImageSource = new BitmapImage(new Uri("Resources/X.png", UriKind.RelativeOrAbsolute));
                 * SocketComm sock = new SocketComm(Settings.ProxiIP, int.Parse(Settings.ProxiPort), SearchTextBox.Text, DataFetched);
                 */
                try
                {
                    log.AddData("Your searched for " + SearchTextBox.Text);
                }
                catch (Exception ee)
                {
                    log.AddData(ee.ToString());
                }
                string query = SearchTextBox.Text;
                SearchTextBox.Text = "";
                // TODO change
                DataFetched(sf("", query), true);
            }

        }
        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            SettingsWindow win = new SettingsWindow();
            win.ShowDialog();
        }

        private void DataFetched(string data, bool isHtml)
        {
            isLoading = false;
            Dispatcher.Invoke(delegate
            {
                tool.ImageSource = new BitmapImage(new Uri("Resources/refresh.png", UriKind.RelativeOrAbsolute));
                if (isHtml)
                    browser.NavigateToString(data);
                else log.AddData(data);
                
            });
        }

        private void Click_Refresh(object sender, RoutedEventArgs args)
        {
            browser.Refresh();
        }
    }
}
