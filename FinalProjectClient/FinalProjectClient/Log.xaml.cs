﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows;
using System.Windows.Controls;

namespace FinalProjectClient
{
    /// <summary>
    /// Interaction logic for Log.xaml
    /// </summary>
    public partial class Log : Page, INotifyPropertyChanged
    {
        bool even;
        string lab = "Number of lines matching selected criteria: ";

        int currentIndx = 0;
        List<IDataRow> lst = new List<IDataRow>();
        ///max rows in one grid
        const int MAX_ROWS = 10;
        public event PropertyChangedEventHandler PropertyChanged;
        public delegate IDataRow newDataRow();
        newDataRow creator;
        public string LabelContent
        {
            get
            {
                if (LstView is null)
                {
                    return "No lines exist";
                }
                return lab + lst.Count;
            }
        }
        public bool NextEnabled
        {
            get
            {
                return ((lst.Count- currentIndx) > 10);
            }
        }
        public bool PrevEnabled
        {
            get
            {
                return (currentIndx >= 10);
            }
        }


        public void setEnvironmentContent(string fHeader, string sHeader,
            string fButtonCont, string sButtonCont, string lab)
        {
            this.fHeader.Content = fHeader;
            this.sHeader.Content = sHeader;
            this.lab = lab;

            fButton.Content = fButtonCont;
            sButton.Content = sButtonCont;
        }
        public Log(newDataRow d)
        {
            creator = d;
            InitializeComponent();
            DataContext = this;
            even = false;
            
        }

        protected virtual void OnPropertyChanged(string property)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }

        public void AddData(string data)
        {
            IDataRow dataRow = creator();
            dataRow.set_content(data, even);

            lst.Insert(0, dataRow);
            // 10 is an arbitrary number
            if (lst.Count < MAX_ROWS || currentIndx == 0)
            {
                LstView.Items.Insert(0, dataRow);
            }

            if(currentIndx == 0 && (lst.Count > MAX_ROWS))
            {
                LstView.Items.RemoveAt(MAX_ROWS);
            }


            even = !even;

            OnPropertyChanged("LabelContent");
            OnPropertyChanged("PrevEnabled");
            OnPropertyChanged("NextEnabled");
        }
     
        private void FetchData_Click(object sender, RoutedEventArgs e)
        {
            // TODO: fix to something more useful
            return;
        }

        private void sButton_Click(object sender, RoutedEventArgs e)
        {
            LstView.Items.Clear();
            currentIndx += MAX_ROWS;
            int n = lst.Count - currentIndx;
            foreach (var v in lst.GetRange(currentIndx, n))
            {
                LstView.Items.Add(v);
            }
            OnPropertyChanged("PrevEnabled");
            OnPropertyChanged("NextEnabled");
        }

        private void fButton_Click(object sender, RoutedEventArgs e)
        {
            LstView.Items.Clear();
            currentIndx -= MAX_ROWS;
            foreach (var v in lst.GetRange(currentIndx, MAX_ROWS))
            {
                LstView.Items.Add(v);
            }
            OnPropertyChanged("PrevEnabled");
            OnPropertyChanged("NextEnabled");
        }
    }
}
