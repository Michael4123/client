﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace FinalProjectClient
{
    public class Settings
    {
        static string _entryGuardIp;
        public static string EntryGuardIP
        {
            get
            {
                return _entryGuardIp;
            }
            set
            {
                tryPutIp(value.ToString(), ref _entryGuardIp);
            }
        }
        static int _entryGuardPort = 0;
        public static string EntryGuardPort
        {
            get
            {
                if (_entryGuardPort <= 0 || _entryGuardPort >= (1 << 16))
                {
                    return "";
                }
                return _entryGuardPort.ToString();
            }
            set
            {
                int.TryParse(value, out _entryGuardPort);
            }
        }
        static string _exitNodeIp;
        public static string ExitNodeIP
        {
            get
            {
                return _exitNodeIp;
            }
            set
            {

                tryPutIp(value.ToString(), ref _exitNodeIp);
            }
        }
        static int _exitNodePort = 0;
        public static string ExitNodePort
        {
            get
            {
                if (_exitNodePort <= 0 || _exitNodePort >= (1 << 16))
                {
                    return "";
                }
                return _exitNodePort.ToString();
            }
            set
            {
                int.TryParse(value, out _exitNodePort);
            }
        }
        public static string ProxiIP
        {
            get
            {
                return ConfigurationManager.AppSettings["IPAddressProxi"];
            }
            set
            {
                string temp = ConfigurationManager.AppSettings["IPAddressProxi"];
                tryPutIp(value.ToString(), ref temp);
                ConfigurationManager.AppSettings["IPAddressProxi"] = temp;
            }
        }

        static public string ProxiPort
        {
            get
            {
                return ConfigurationManager.AppSettings["portnoProxi"];
            }
            set
            {
                int res;
                if (int.TryParse(value, out res))
                {
                    if (res >= 0 && res < (1 << 16))
                    ConfigurationManager.AppSettings["portnoProxi"] = res.ToString();
                }
            }
        }

        public static string DirIP
        {
            get
            {
                return ConfigurationManager.AppSettings["DirectoryIP"];
            }
            set
            {
                string temp = ConfigurationManager.AppSettings["DirectoryIP"];
                tryPutIp(value.ToString(), ref temp);
                ConfigurationManager.AppSettings["DirectoryIP"] = temp;
            }
        }

        static public string DirPort
        {
            get
            {
                return ConfigurationManager.AppSettings["DirectoryPort"];
            }
            set
            {
                int res;
                if (int.TryParse(value, out res))
                {
                    if (res >= 0 && res < (1 << 16))
                        ConfigurationManager.AppSettings["DirectoryPort"] = res.ToString();
                }
            }
        }

        static bool tryPutIp(string val, ref string ip)
        {
            string[] vals = val.Split('.');
            if (vals.Length != 4) return false;
            foreach (var v in vals)
            {
                int temp;
                if (!int.TryParse(v, out temp)) return false;
                if (temp < 0 || temp > 255) return false;
            }

            ip = val;
            return true;
        }

        public static int localPort
        {
            get
            {
                return int.Parse(ConfigurationManager.AppSettings["LocalClientPort"]);
            }
        }
    }
}
