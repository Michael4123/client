﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FinalProjectClient
{
    /// <summary>
    /// Interaction logic for ImageButton.xaml
    /// </summary>
    public partial class ImageButton : UserControl
    {
        public object ImageSource
        {
            get { return GetValue(ValueProperty); }
            set { SetValue(ValueProperty, value); }
        }
        public object InnerPadding
        {
            get { return GetValue(InPaddingProperty); }
            set { SetValue(InPaddingProperty, value); }
        }
        public object InnerMargin
        {
            get { return GetValue(InMarginProperty); }
            set { SetValue(InMarginProperty, value); }
        }
        public static readonly DependencyProperty ValueProperty =
            DependencyProperty.Register("ImageSource", typeof(object),
            typeof(ImageButton), new PropertyMetadata(null));

        public static readonly DependencyProperty InPaddingProperty =
            DependencyProperty.Register("InnerPadding", typeof(object),
            typeof(ImageButton), new PropertyMetadata(null));

        public static readonly DependencyProperty InMarginProperty =
            DependencyProperty.Register("InnerMargin", typeof(object),
            typeof(ImageButton), new PropertyMetadata(null));
        
        public ImageButton()
        {
            InitializeComponent();
            LayoutRoot.DataContext = this;
        }
        
    }
}
