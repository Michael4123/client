﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalProjectClient.OnionRouting
{
    /// <summary>
    /// This class is intended to keep a dynamic cache, i.e. 
    /// one that refreshes every once in a while and maybe saved
    /// some data in external storage, but for now it just acts
    /// as a regular dictionary
    /// </summary>
    class Cache<T, K>
    {
        Dictionary<T, K> cache = new Dictionary<T, K>();

        public void put(T key, K val)
        {
            cache[key] = val;
        }

        public K get(T rout)
        {
            if (cache.ContainsKey(rout))
            {
                return cache[rout]; 
            }
            return default(K);
        }
    }
}
