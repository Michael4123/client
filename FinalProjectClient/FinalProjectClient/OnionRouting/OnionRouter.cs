﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FinalProjectClient.CommConv;

namespace FinalProjectClient.OnionRouting
{
    class InsufficientArguments : Exception { }
    /// <summary>
    /// Class that makes the actual onion communication
    /// </summary>
    class OnionRouter
    {
        OnionSession os;
        CommConv.RouterData[] path;
        Cache<string, string> cache = new Cache<string, string>();
        protected const int num_routers = 3;

        public OnionRouter()
        {
            os = new OnionSession("127.0.0.1", Settings.localPort);
        }
        /// <summary>
        /// This method ideally should consider the adding date, 
        /// verification date, and of course the user request
        /// </summary>
        protected void ChoosePath(IReadOnlyCollection<CommConv.RouterData> routers)
        {
            Random r = new Random();
            int[] nums = new int[num_routers];
            for (int i = 0; i < num_routers; ++i)
            {
                nums[i] = r.Next() % (routers.Count - i);
                for (int j = 0; j < i; ++j)
                {
                    if (nums[i] >= nums[j]) nums[i]++;
                }
            }

            path = new CommConv.RouterData[num_routers];
            for (int i = 0; i < num_routers; ++i)
            {
                path[i] = routers.ElementAt(nums[i]);
            }

        }

        protected void SetPK()
        {
            foreach (CommConv.RouterData r in path)
            {
                string s = AddressToStr(r.ip, (short) r.port);
                if (cache.get(s) == null)
                {
                    ServerCommand scc = new GetPKCommand(r.ip, (short) r.port);
                    scc.SendData();
                    cache.put(s, scc.Data);
                }
            }
        }
        void AddPKServer()
        {
            foreach(CommConv.RouterData r in path)
            {
                string s = AddressToStr(r.ip, (short)r.port);
                ServerCommand pk = new AddPKCommand(os, r.ip, (short)r.port, cache.get(s));
                pk.SendData();
            }
        }
        protected string AddressToStr(string ip, short port)
        {
            return StringServerData.ipToInt(ip).ToString() + port.ToString();
        } 

        void AddRouters()
        {
            foreach (CommConv.RouterData r in path)
            {
                string s = AddressToStr(r.ip, (short)r.port);
                ServerCommand pk = new AddRoutCommand(os, r.ip, (short)r.port);
                pk.SendData();
            }
        }
        public string SendAndRecv(IReadOnlyCollection<CommConv.RouterData> routers, string s,
            string ip, short port, bool ssl = false)
        {
            ChoosePath(routers);
            if (routers.Count < num_routers)
            {
                throw new InsufficientArguments();
            }
            SetPK();
            AddPKServer();
            AddRouters();
            ServerCommand ad = new AddDstCommand(os, ip, port);
            ad.SendData();

            ServerCommand cmd = new SendCommand(os, s);
            cmd.SendData();
            return cmd.Data;
        }
    }
}
