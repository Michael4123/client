﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace FinalProjectClient
{
    delegate void dataFetched(string ob, bool isHtml);
    class SocketComm
    {
        string ip;
        string data;
        int portno;
        public event dataFetched dataFetchedEventHandler;
        public SocketComm(string ipv4, int portno, string data, dataFetched op)
        {
            dataFetchedEventHandler += op;
            ip = ipv4;
            this.portno = portno;
            this.data = data;
            Thread thread = new Thread(connect);
            thread.Start();
        }
        public void connect()
        {
            Socket sock = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            sock.Connect(IPAddress.Parse(ip), portno);

            // This operation is sequential
            byte[] toBytes = Encoding.ASCII.GetBytes(data);
            sock.Send(toBytes);

            toBytes = new byte[4];
            sock.Receive(toBytes);
            int len = BitConverter.ToInt32(toBytes, 0);
            sock.Receive(toBytes);
            bool isHtml = (BitConverter.ToInt32(toBytes, 0) == 0);
            toBytes = new byte[len];
            sock.Receive(toBytes);
            sock.Close();
           
            string something = Encoding.ASCII.GetString(toBytes, 0, len);

            dataFetchedEventHandler(something, isHtml);
        }
    }
}
