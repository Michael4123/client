﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FinalProjectClient
{
    /// <summary>
    /// Interaction logic for SettingsWindow.xaml
    /// </summary>
    public partial class SettingsWindow : Window, INotifyPropertyChanged
    {
        Configuration config;

        public event PropertyChangedEventHandler PropertyChanged;

        public SettingsWindow()
        {
            InitializeComponent();

            config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            DataContext = new Settings();
        }

        
        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            Settings.ExitNodeIP = ExitNode.IP as string;
            Settings.ExitNodePort = ExitNode.Port as string;
            Settings.DirIP = Dir.IP as string;
            Settings.DirPort = Dir.Port as string;
            Settings.EntryGuardIP = EntryGuard.IP as string;
            Settings.EntryGuardPort = EntryGuard.Port as string;
            Settings.ProxiIP = Proxi.IP as string;
            Settings.ProxiPort = Proxi.Port as string;
            Close();
        }
        protected virtual void OnPropertyChanged(string property)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }


        protected override void OnClosing(CancelEventArgs e)
        {
            config.Save(ConfigurationSaveMode.Modified);
            
            base.OnClosing(e);
        }
    }
}
