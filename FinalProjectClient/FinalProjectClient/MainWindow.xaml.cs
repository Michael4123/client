﻿using System.Windows;
using FinalProjectClient.CommConv;
using FinalProjectClient.OnionRouting;

namespace FinalProjectClient
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Cache<string, string> dns = new Cache<string, string>();
        public delegate string searchFunc(string n, string q);
        OnionRouter or;
        GetRoutersCommand sc;
        public string Copyright
        {
            get { return "Michael & Dror"; }
        }
        public MainWindow()
        {
            InitializeComponent();
            DataContext = this;

            Log log = new Log(delegate () { return new DataRow(); });
            logFrame.Content = log;

            Log routers = new Log(delegate () { return new RouterData(); });
            routers.setEnvironmentContent("IP", "Port", "back", "next", "Number of routers in the net: ");
            routFrame.Content = routers;

            Search searcher = new Search(log, Search);
            searchFrame.Content = searcher;

            sc = new GetRoutersCommand();
            sc.SendData();
            foreach (var s in sc.Routers)
            {
                routers.AddData(s.ToString());
            }
        }

        private string Search(string domain, string query)
        {
            string ip = null;
            int port = 0;
            if (dns.get(domain) == null)
            {
                ip = "127.0.0.1";
                port = 4043;
            }
            else
            {
                string[] s = dns.get(domain).Split(',');
                ip = s[0];
                port = int.Parse(s[1]);
            }
            or = new OnionRouter();
            return or.SendAndRecv(sc.Routers, query, ip, (short) port);
        }

    }
}
