﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace FinalProjectClient.CommConv
{
    enum OnionRespCodes
    {
        OK = 1,
        SEND_PK
    }
    abstract class OnionCommand : ServerCommand
    {
        OnionSession os;
        abstract public OnionRespCodes Code { get; }
        public OnionCommand(ACTION_ID command_id, OnionSession os) : base(command_id)
        {
            this.os = os;
        }
        override public void SendData(Socket sock = null)
        {
            if (sock == null)
                base.SendData(os.sock);
            else base.SendData(sock);
        }
        public override string Data
        {
            get
            {
                return Code.ToString();
            }
        }

    }
}
