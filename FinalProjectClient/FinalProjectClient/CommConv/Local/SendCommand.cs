﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace FinalProjectClient.CommConv
{
    class SendCommand : OnionCommand
    {
        public SendCommand(OnionSession os, string msg) : base(ACTION_ID.SEND_LOC, os)
        {
            data_params.Add(new StringServerData(msg));
        }
        string data;
        public override string Data
        {
            get
            {
                return data;
            }
        }
        public override OnionRespCodes Code => throw new NotImplementedException();

        protected override void onFinishedWriting(Socket sock)
        {
            data = StringServerData.read(sock);
        }
    }
}
