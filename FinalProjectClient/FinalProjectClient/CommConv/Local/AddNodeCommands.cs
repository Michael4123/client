﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace FinalProjectClient.CommConv
{
    abstract class LocalCommand : OnionCommand
    {
        public LocalCommand(OnionSession os, ACTION_ID id, string ip, short port) : base(id, os)
        {
            data_params.Add(new IntServerData(StringServerData.ipToInt(ip)));
            data_params.Add(new ShortServerData(port));
        }
    }
    class AddRoutCommand : LocalCommand
    {
        public AddRoutCommand(OnionSession os, string ip, short port) : base(os, ACTION_ID.ADD_ROUT_LOC, ip, port) { }

        OnionRespCodes code;
        public override OnionRespCodes Code
        {
            get
            {
                return code;
            }
        }

        protected override void onFinishedWriting(Socket sock)
        {
            byte[] buff = new byte[1];
            sock.Receive(buff);
            switch (buff[0])
            {
                case 1:
                    code = OnionRespCodes.OK;
                    break;
                case 2:
                    code = OnionRespCodes.SEND_PK;
                    break;
                default:
                    throw new FormatException();

            }
        }
    }

    class AddDstCommand : LocalCommand
    {
        public AddDstCommand(OnionSession os, string ip, short port) : base(os, ACTION_ID.ADD_DST_LOC, ip, port) { }

        OnionRespCodes code;
        public override OnionRespCodes Code
        {
            get
            {
                return code;
            }
        }


        protected override void onFinishedWriting(Socket sock)
        {
            byte[] buff = new byte[1];
            sock.Receive(buff);
            switch (buff[0])
            {
                case 1:
                    code = OnionRespCodes.OK;
                    break;
                default:
                    throw new FormatException();

            }
        }
    }
}
