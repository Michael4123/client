﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace FinalProjectClient.CommConv
{
    class AddPKCommand : OnionCommand
    {
        public AddPKCommand(OnionSession os, string ip, short port, string pk)
            : base(ACTION_ID.ADD_PK_LOC, os)
        {
            data_params.Add(new IntServerData(StringServerData.ipToInt(ip)));
            data_params.Add(new ShortServerData(port));
            data_params.Add(new StringServerData(pk));
        }

        OnionRespCodes code;
        public override OnionRespCodes Code
        {
            get
            {
                return code;
            }
        }

        protected override void onFinishedWriting(Socket sock)
        {
            byte[] b = new byte[1];
            sock.Receive(b);
            if (b[0] != 1)
            {
                throw new FormatException();
            }
        }
    }
}
