﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace FinalProjectClient.CommConv
{
    class ShortServerData : IServerData
    {
        short val;
        public int Length
        {
            get
            {
                return 2;
            }
        }
        public ShortServerData(short val)
        {
            this.val = val;
        }

        public object getVal()
        {
            return val;
        }

        public byte[] get_bytes()
        {
            // exists a faster solution - using unsafe code
            return BitConverter.GetBytes(val);
        }

        public void send(Socket os)
        {
            os.Send(get_bytes());
        }
    }
}
