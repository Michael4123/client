﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace FinalProjectClient.CommConv
{
    public delegate void DataReceived(Socket s);
    class SendServer
    {
        public event DataReceived Data_recv;
        string ip;
        int port;

        public SendServer(string ip, int port)
        {
            this.ip = ip;
            this.port = port;
        }

        public void SendToServerRaw(Socket sock, IEnumerable<IServerData> data)
        {
            short len = 0;
            for (int i = 0; i < data.Count(); ++i)
            {
                len += (short)data.ElementAt(i).Length;
            }
            ShortServerData ssd = new ShortServerData(len);

            ssd.send(sock);
            foreach (IServerData d in data)
            {
                d.send(sock);
            }

        }
        /// <summary>
        /// a general method to send requests to server
        /// </summary>
        /// <param name="data">array of ServerData to send</param>
        /// <returns></returns>
        public void SendToServer(Socket sock, IEnumerable<IServerData> data)
        {
            bool close = false;
            if (sock == null)
            {
                sock = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                sock.Connect(IPAddress.Parse(ip), port);
            }
            SendToServerRaw(sock, data);
            Data_recv?.Invoke(sock);
            if (close)
            {
                sock.Close();
            }
        }
    }
}
