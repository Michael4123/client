﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace FinalProjectClient.CommConv
{
    /// <summary>
    /// interface for sending data
    /// </summary>
    interface IServerData
    {
        /// <summary>
        /// sending value through socket
        /// </summary>
        /// <param name="ou">open socket to write data</param>
        void send(Socket os);

        /// <summary>
        /// get value as bytes array
        /// </summary>
        /// <returns>byte array</returns>
        byte[] get_bytes();

        /// <summary>
        /// get value return object because type is unknown
        /// </summary>
        /// <returns>value</returns>
        Object getVal();

        /// <summary>
        /// return value packed length
        /// </summary>
        /// <returns>the length of the value in bytes</returns>
        int Length { get; }
    }

}
