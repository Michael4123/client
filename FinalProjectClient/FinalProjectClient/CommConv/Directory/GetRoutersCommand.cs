﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace FinalProjectClient.CommConv
{
    struct RouterData
    {
        public string ip;
        public int port;
        public string add_date;
        public string last_ver;
        override public string ToString()
        {
            return String.Join(",", ip, port.ToString(), add_date, last_ver);
        }
    }
    class GetRoutersCommand : ServerCommand
    {
        RouterData[] routers;
        public IReadOnlyCollection<RouterData> Routers
        {
            get
            {
                return routers;
            }
        }
        string s = "";
        public override string Data
        {
            get
            {
                return s;
            }
        }
       

        public GetRoutersCommand() : base(ACTION_ID.GET_ROUTER_CLIENT_ID, 
            Settings.DirIP, int.Parse(Settings.DirPort))
        {

        }

        protected override void onFinishedWriting(Socket sock)
        {
            List<RouterData> lst = new List<RouterData>();
            int code = IntServerData.read(sock);

            while (code != 0)
            {
                RouterData rb = new RouterData();
                // This is a waste of time, but more elegant
                rb.ip = (StringServerData.intToIp(code));
                rb.port = IntServerData.read(sock);

                rb.add_date = StringServerData.read(sock);
                rb.last_ver = StringServerData.read(sock);

                lst.Add(rb);

                code = IntServerData.read(sock);
            }
            routers = lst.ToArray();
        }
    }
}
