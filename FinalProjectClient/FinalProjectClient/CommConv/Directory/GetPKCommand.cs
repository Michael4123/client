﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace FinalProjectClient.CommConv
{
    class GetPKCommand : ServerCommand
    {
        public GetPKCommand(string ip, short port) : base(ACTION_ID.GET_ROUTER_PK_ID,Settings.DirIP, int.Parse(Settings.DirPort))
        {
            data_params.Add(new IntServerData(StringServerData.ipToInt(ip)));
            data_params.Add(new ShortServerData(port));
        }

        string pk;
        public override string Data
        {
            get
            {
                return pk;
            }
        }

        protected override void onFinishedWriting(Socket sock)
        {
            pk = StringServerData.read(sock);
        }
    }
}
