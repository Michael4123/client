﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace FinalProjectClient.CommConv
{
    public class IntServerData : IServerData
    {
        private int val;
        public IntServerData(int val)
        {
            this.val = val;
        }

        public int Length
        {
            get
            {
                return 4;
            }
        }

        public object getVal()
        {
            return val;
        }

        public byte[] get_bytes()
        {
            // exists a faster solution - using unsafe code
            return BitConverter.GetBytes(val);
        }

        public void send(Socket os)
        {
            os.Send(get_bytes());
        }

        public static int read(Socket os)
        {
            byte[] buffer = new byte[4];
            os.Receive(buffer);
            return BitConverter.ToInt32(buffer, 0);
        }
    }
}
