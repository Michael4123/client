﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace FinalProjectClient.CommConv
{
    class StringServerData : IServerData
    {
        const int MAX_STR_LEN = 4;
        string s;
        public StringServerData(string s)
        {
            this.s = s;
        }
        public int Length
        {
            get
            {
                return s.Length + MAX_STR_LEN;
            }
        }
        public object getVal()
        {
            return s;
        }

        public byte[] get_bytes()
        {
            IntServerData isd = new IntServerData(s.Length);
            byte[] int_b = isd.get_bytes();
            byte[] toBytes = Encoding.UTF8.GetBytes(s);

            var z = new byte[int_b.Length + toBytes.Length];
            int_b.CopyTo(z, 0);
            toBytes.CopyTo(z, int_b.Length);

            return z;
        }

        public IEnumerable<byte[]> _get_bytes_fast()
        {
            IntServerData isd = new IntServerData(s.Length);
            byte[] int_b = isd.get_bytes();
            byte[] toBytes = Encoding.UTF8.GetBytes(s);

            yield return int_b;
            yield return toBytes;
        }

        public void send(Socket os)
        {
            foreach (byte[] b in _get_bytes_fast())
            {
                os.Send(b);
            }
        }

        public static string read(Socket os)
        {
            int len = IntServerData.read(os);
            if (len == 0) return "";
            byte[] buffer = new byte[len];
            os.Receive(buffer);
            return Encoding.ASCII.GetString(buffer);
        }

        public static string intToIp(int ip)
        {
            byte[] ins = BitConverter.GetBytes(ip);

            StringBuilder ip_s = new StringBuilder();
            for (int i = 0; i < 4; ++i)
            {
                ip_s.Append(((ins[i] + 256) % 256).ToString());
                ip_s.Append(".");
            }
            ip_s.Remove(ip_s.Length - 1, 1);
            return ip_s.ToString();
        }

        public static int ipToInt(string ip)
        {
            byte[] b = new byte[4];
            int i = 0;
            foreach (var n in ip.Split('.'))
            {
                b[i++] = (byte) int.Parse(n);
            }
            return BitConverter.ToInt32(b, 0);
        }
    }
}
