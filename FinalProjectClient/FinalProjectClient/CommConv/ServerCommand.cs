﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace FinalProjectClient.CommConv
{
    abstract class ServerCommand
    {
        public abstract string Data
        {
            get;
        }
        protected List<IServerData> data_params;
        string ip;
        int port;
        short command_id;
        short COMMAND_ID
        {
            get { return command_id; }
        }
        protected ServerCommand(ACTION_ID command_id)
        {
            this.command_id = (short) command_id;
            data_params = new List<IServerData>();
            port = -1;
        }
        protected ServerCommand(ACTION_ID command_id, string ip, int port)
        {
            data_params = new List<IServerData>();
            this.command_id = (short) command_id;
            this.ip = ip;
            this.port = port;
        }

        abstract protected void onFinishedWriting(Socket sock);

        virtual public void SendData(Socket sock = null)
        {
            if (port == -1 && sock == null) throw new ArgumentNullException();
            SendServer ss = new SendServer(ip, port);
            ss.Data_recv += onFinishedWriting;
            data_params.Insert(0, new ShortServerData(command_id));
            ss.SendToServer(sock, data_params);
            ss.Data_recv -= onFinishedWriting;
        }
    }
}
