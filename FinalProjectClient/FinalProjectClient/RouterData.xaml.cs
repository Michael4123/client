﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FinalProjectClient
{
    /// <summary>
    /// Interaction logic for RouterData.xaml
    /// </summary>
    public partial class RouterData : UserControl, IDataRow
    {
        public object IP
        {
            get { return GetValue(IPProperty); }
            set { SetValue(IPProperty, value); }
        }
        public object Port
        {
            get { return GetValue(PortProperty); }
            set { SetValue(PortProperty, value); }
        }
        public object AddDate
        {
            get { return GetValue(DateProperty); }
            set { SetValue(DateProperty, value); }
        }
        public object VerDate
        {
            get { return GetValue(VerProperty); }
            set { SetValue(VerProperty, value); }
        }
        public bool EvenRow
        {
            get { return (bool)GetValue(EvenProperty); }
            set { SetValue(EvenProperty, value); }
        }
        public object RowColor
        {
            get
            {
                SolidColorBrush brush;
                if (EvenRow) brush = new SolidColorBrush(Colors.LightGray);
                else brush = new SolidColorBrush(Colors.WhiteSmoke);
                return brush;
            }
        }


        public static readonly DependencyProperty IPProperty =
            DependencyProperty.Register("IP", typeof(object),
            typeof(Label), new PropertyMetadata(null));

        public static readonly DependencyProperty PortProperty =
            DependencyProperty.Register("Port", typeof(object),
            typeof(Label), new PropertyMetadata(null));

        public static readonly DependencyProperty DateProperty =
            DependencyProperty.Register("AddDate", typeof(object),
            typeof(Label), new PropertyMetadata(null));

        public static readonly DependencyProperty VerProperty =
            DependencyProperty.Register("VerDate", typeof(object),
            typeof(Label), new PropertyMetadata(null));

        public static readonly DependencyProperty EvenProperty =
            DependencyProperty.Register("EvenRow", typeof(object),
            typeof(Label), new PropertyMetadata(null));
        public RouterData()
        {
            InitializeComponent();
            DataContext = this;
        }

        public void set_content(string s, bool even = false)
        {
            string[] data = s.Split(',');
            IP = data[0];
            Port = data[1];
            AddDate = data[2];
            VerDate = data[3];

            Margin = new Thickness(0, 0, 0, 0);
            EvenRow = even;
        }

        public string get_content()
        {
            throw new NotImplementedException();
        }
    }
}
