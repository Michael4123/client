﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FinalProjectClient
{
    /// <summary>
    /// Interaction logic for DataRow.xaml
    /// </summary>
    public partial class DataRow : UserControl, IDataRow
    {
        public object Date
        {
            get { return GetValue(DateProperty); }
            set { SetValue(DateProperty, value); }
        }
        public object Data
        {
            get { return GetValue(DataProperty); }
            set { SetValue(DataProperty, value); }
        }
        public bool EvenRow
        {
            get { return (bool) GetValue(EvenProperty); }
            set { SetValue(EvenProperty, value); }
        }
        public object RowColor
        {
            get
            {
                SolidColorBrush brush;
                if (EvenRow) brush = new SolidColorBrush(Colors.LightGray);
                else brush = new SolidColorBrush(Colors.WhiteSmoke);
                return brush;
            }
        }


        public static readonly DependencyProperty DateProperty =
            DependencyProperty.Register("Date", typeof(object),
            typeof(ImageButton), new PropertyMetadata(null));
        public static readonly DependencyProperty DataProperty =
            DependencyProperty.Register("Data", typeof(object),
            typeof(ImageButton), new PropertyMetadata(null));
        public static readonly DependencyProperty EvenProperty =
            DependencyProperty.Register("EvenRow", typeof(object),
            typeof(ImageButton), new PropertyMetadata(null));
        public DataRow()
        {
            InitializeComponent();
            RowName.DataContext = this;
        }

        public void set_content(string data, bool even)
        {
            Data = data;

            StringBuilder temp = new StringBuilder(DateTime.Now.ToString());
            // Adding newline after the date
            for (int i = 0; i < temp.Length; ++i)
            {
                if (temp[i] == ' ')
                {
                    temp[i] = '@';
                    break;
                }
            }
            temp = temp.Replace("@", Environment.NewLine);
            Date = temp;

            Margin = new Thickness(0, 0, 0, 0);
            EvenRow = even;
        }

        public string get_content()
        {
            throw new NotImplementedException();
        }
    }
}
