﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FinalProjectClient
{
    /// <summary>
    /// Interaction logic for NodeContent.xaml
    /// </summary>
    public partial class NodeContent : UserControl
    {
        public object NodeName
        {
            get { return GetValue(NodeNameProp); }
            set { SetValue(NodeNameProp, value); }
        }
        public object IP
        {
            get { return GetValue(IPProp); }
            set { SetValue(IPProp, value); }
        }
        public object Port
        {
            get { return GetValue(PortProp); }
            set { SetValue(PortProp, value); }
        }



        public static readonly DependencyProperty NodeNameProp =
            DependencyProperty.Register("NodeName", typeof(object),
            typeof(NodeContent));
        public static readonly DependencyProperty IPProp =
            DependencyProperty.Register("IP", typeof(object),
            typeof(NodeContent));
        public static readonly DependencyProperty PortProp =
            DependencyProperty.Register("Port", typeof(object),
            typeof(NodeContent));
        public NodeContent()
        {
            InitializeComponent();

            DataContext = new SettingsDC(this);
        }
    }
}
